---
layout: archives
---

# Docs archives

CAUTION: **Warning:**
This page is in beta, many links might not work. For updates, follow
[this issue](https://gitlab.com/gitlab-com/gitlab-docs/issues/16).

Browse the archives for different GitLab versions.

## 10.5

Visit the [GitLab 10.5 docs](/10.5/) or download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.5
```

## 10.4

Visit the [GitLab 10.4 docs](/10.4/) or download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.4
```

## 10.3

Visit the [GitLab 10.3 docs](/10.3/) or download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.3
```
